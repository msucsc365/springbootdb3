<!doctype html>
<html lang="en">
<%@ include file="/WEB-INF/layouts/include.jsp" %>
<%@ include file="/WEB-INF/layouts/head.jsp" %>
<body id="demo-body">
	<div id="demo-main-div" class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<%-- Title --%>
				<h1>Example Page</h1>
				
				<%-- User Message --%>
				<%@ include file="/WEB-INF/layouts/message.jsp" %>
				
				<%-- Add User Button --%>
				<div class="row mt-2">
					<div class="col-3">
						<a class="btn btn-success" href="<%=request.getContextPath() %>/users/addEditUser">Add User</a>
					</div>
				</div>
				
				<%-- Users --%>
				<div class="row mt-3">
					<div class="col-12">
						<%-- No Records Found --%>
						<c:if test="${empty userList}">
							No Records Found
						</c:if>
						
						<%-- Records Found --%>
						<c:if test="${not empty userList}">
							<c:forEach var="user" items="${userList}">
							    <c:out value="${user}"/><br/> 
							</c:forEach>
						</c:if>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</body>
</html>