<!doctype html>
<html lang="en">
<%@ include file="/WEB-INF/layouts/include.jsp" %>
<%@ include file="/WEB-INF/layouts/head.jsp" %>
<body id="demo-body">
	<div id="demo-main-div" class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<h1>${title}</h1>
				<%@ include file="/WEB-INF/layouts/message.jsp" %>
				<%-- 
					The Add/Edit User Form
					We are using ${pageContext.request.contextPath} to reference the context path.
					The order of parsing does not allow us to use <%=request.getContextPath() %>  
				--%>
				<form:form method="post" id="userForm" modelAttribute="command" 
					action="${pageContext.request.contextPath}/users/addEditUser">
					
					<div class="form-group col-sm-3">
						<%--
							The label and input for firstName 
							path="firstName" is converted to: 
								id="firstName" name="firstName" 
							in the HTML 
						 --%>
						<label for="firstName">First Name</label>
						<form:input class="form-control" path="firstName" placeholder="Enter First Name" />
						
						<%--
							The label and input for lastName
						 --%>	
						<label for="lastName" class="mt10">Last Name</label>
						<form:input class="form-control" path="lastName" placeholder="Enter Last Name" />
						
						<%-- 
							The submit button. Notice that the type is "button" and not submit. We do 
							not want to submit the form when the button is clicked. Instead we want to
							validate the form (client-side), and then send the form to the server. 
						--%>
						<button class="btn btn-primary mt-3" type="button" id="submitBtn">
							Submit
						</button>
					</div>
				</form:form>
			</div>
		</div>
	</div>
	<style>
		#userForm .form-group {
			padding-left: 0px !important;
		}
	</style>
	<script>
		// Client Side Validation
		window.addEventListener("DOMContentLoaded", (event) => {
			
			// Get access to submit button element 
			let submitBtn = document.getElementById("submitBtn")
			
			// Handle/Process Submit Button Click
			submitBtn.addEventListener("click", (event) => {
				// Stop old versions of IE from processing the form 
				// even though the button type is button
				event.preventDefault();
				
				// Variables
				let firstName = document.getElementById("firstName").value;
				let lastName = document.getElementById("lastName").value;
				let form = document.getElementById("userForm"); 
				
				if (firstName.length == 0 || lastName.length == 0) {
					alert("Error: Please enter a first and last name.");
				} else {
					// The next line of code  will work when there is only one 
					// form to process. Kind of dangerous to do though...
					//document.forms[0].submit();
					
					// Submit the form to the server
					form.submit();
				}
			});
		});
	</script>
</body>
</html>