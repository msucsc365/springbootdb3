package edu.missouristate.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springbootdb3Application {

	public static void main(String[] args) {
		SpringApplication.run(Springbootdb3Application.class, args);
	}

}
