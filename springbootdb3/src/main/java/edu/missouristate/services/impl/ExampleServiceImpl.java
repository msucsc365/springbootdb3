package edu.missouristate.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.missouristate.dao.UserRepository;
import edu.missouristate.model.User;
import edu.missouristate.services.UserService;

@Service("exampleService")
public class ExampleServiceImpl implements UserService {
    
    @Autowired
    UserRepository userRepo;

	@Override
	public List<User> getUsers() {
		return userRepo.getUsers();
	}

	@Override
	@Transactional
	public void addUser(User user) {
		userRepo.addUser(user);
	}
   
}
