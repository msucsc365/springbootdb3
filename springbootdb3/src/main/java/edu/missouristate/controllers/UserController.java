package edu.missouristate.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import edu.missouristate.model.User;
import edu.missouristate.services.UserService;

@Controller
public class UserController {

	@Autowired
	UserService userService;
	
	@GetMapping("/users")
	public String getUserTable(Model model) {
		List<User> userList = userService.getUsers();
		model.addAttribute("userList", userList);
		return "users";
	}
	
	@GetMapping("/users/addEditUser")
	public String getAddEditUser(Model model, String id) {
		// Create a new instance of User
		User user = new User();
		
		// Add the empty user and title to the model
		model.addAttribute("command", user);
		model.addAttribute("title", "Add User");
		
		// Return the Add Edit JSP
		return "addEditUser";
	}
	
	@PostMapping("/users/addEditUser")
	public String postAddEditUser(Model model, User user) {
		userService.addUser(user);
		return "redirect:/users";
	}
	
}
